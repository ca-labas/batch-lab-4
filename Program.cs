﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Globalization;
using System.Diagnostics.Contracts;

namespace lab4
{
    enum BadDateHandle : byte { SkipFile, SkipDate, RoundDate, Error }

    enum ErrorCodes : int { InvalidArgument=160, InvalidParameter=87, TooFewParameters=3601, DirNotFound=3, InvalidFunction=1 }
    class Program
    {
        static int?[] DateTimeComponents;
        
        static string ReturnHelpText()
        {
            return "Cинтаксис запису параметрів:" +
                   "\n<шлях до папки> (-d <день>|. <місяць>|. <рік>|.)+(-t <год>|. <хв>|. <сек>|. ) " +
                   "[-nosd] [-ext {<розширення1>, ...}] [-bd skf|skd|rnd|err] [-sk {r, h, s, a}]" +
                   "    * Можна задати зміну дати з часом, тільки дати чи тільки часу\n" +
                   "    * День, місяць та компоненти часу можна писати як з початковим нулем, так і без\n" +
                   "    * Крапка замість компоненту дати/часу означає, що його не треба змінювати\n" +
                   "    * Одразу всі 3 компоненти дати/часу НЕ можуть бути пропущені крапками\n" +
                   "-----------------------------\n" +
                   "-nosd (\"no subdirectories\") - не чіпати файли у підпапках\n" +
                   "-----------------------------\n" +
                   "-ext (\"extensions\") - зазначає, з файлами яких розширень працювати\n" +
                   "    * Розширень може бути довільна кількість\n" +
                   "    * Розширення можна писати як із крапкою попереду, так і без: наприклад, .exe або exe (АЛЕ не *.exe)\n" +
                   "    * Без цього параметра програма працюватиме з файлами з будь-якими розширеннями\n" +
                   "-----------------------------\n" +
                   "-bd (\"bad date\") - що робити у випадку утворення недопустимої дати при частковій зміні дати створення\n" +
                   "    Наприклад, дата створення файлу 31.01.2023, запускаємо програму з параметром -d . 06 2022\n" +
                   "    Введення є припустимим, але для даного конкретного файлу утворюється значення дати 31.06.2022.\n" +
                   "    Що робити в таких випадках - зазначте 1 опцію:\n" +
                   "    * skf (\"skip file\") - пропустити всю обробку файлу\n" +
                   "    * skd (\"skip date\") ЗА ЗАМОВЧЕННЯМ - пропустити зміну дати, але змінювати час створення, якщо він заданий\n" +
                   "    * rnd (\"round\") - виправити дату на найближчу допустиму, наприклад 31.06 на 30.06, 29.02.2021 на 28.02.2021\n" +
                   "    * err (\"error\") - викинути помилку та закінчити виконання програми\n" +
                   "-----------------------------\n" +
                   "-sk (\"skip\") - пропускати файли з певними атрибутами (можна зазначити декілька):\n" +
                   "    * r - тільки для читання\n" +
                   "    * h - прихований\n" +
                   "    * s - системний\n" +
                   "    * a - архівний\n";
        }

        static bool FilterOutInvalidDate(string[] tokens_here, int tokenD_position)
        {
            int day, month, year;
            bool fail1, fail2, fail3;
            fail1 = fail2 = fail3 = true;

            string day_tk = tokens_here[tokenD_position + 1],
                   mon_tk = tokens_here[tokenD_position + 2],
                   y_tk = tokens_here[tokenD_position + 3];

            if (int.TryParse(day_tk, out day))
            {
                fail1 = day <= 0 || day > 31;
                if (fail1) {
                    Console.WriteLine("Помилка - день може бути в інтервалі від 1 до 31 включно");
                }
            }
            else {
                fail1 = day_tk != ".";
                if (fail1) {
                    Console.WriteLine($"Помилка - значення для дня {tokens_here[tokenD_position + 1]} не є числом!");
                }  
            }

            if (int.TryParse(mon_tk, out month))
            {
                fail2 = month <= 0 || month > 12;
                if (fail2) {
                    Console.WriteLine("Помилка - місяць може бути в інтервалі від 1 до 12 включно");
                }
            }
            else {
                fail2 = mon_tk != ".";
                if (fail2) { 
                    Console.WriteLine($"Помилка - значення для місяця {tokens_here[tokenD_position + 2]} не є числом!");
                }
            }

            if (int.TryParse(y_tk, out year))
            {
                fail3 = year < 1601 || year > 9999;
                if (fail3) {
                    Console.WriteLine("Помилка - у Windows підтримуються роки від 1601 до 9999 включно");
                }
            }
            else {
                fail3 = y_tk != ".";
                if (fail3) {
                    Console.WriteLine($"Помилка - значення для року {tokens_here[tokenD_position + 3]} не є числом!");
                } 
            }

            if(day==0 && month==0 && year==0) {
                Console.WriteLine("Помилка - всі компоненти дати пропущені!"); return true;
            }

            if (CheckPotentialDate(day, month, year)) {

                if (day > 0) { DateTimeComponents[0] = day; }
                if (month > 0) { DateTimeComponents[1] = month; }
                if (year > 0) { DateTimeComponents[2] = year; }
            }
            else { Console.WriteLine($"Помилка - {day}.{month:00}.{year} - недопустима дата!"); return true; }

            return fail1 || fail2 || fail3;
        }
        
        static bool CheckPotentialDate(int d, int m, int y)
        {
            if (m == 2 && (d > 28 && y % 4 != 0 || d > 29 && y % 4 == 0)) { return false; }

            else if (d == 31 && (m == 4 || m == 6 || m == 9 || m == 11)) { return false; }
            
            return true;
        }

        static bool FilterOutInvalidTime(string[] tokens_here, int tokenTposition)
        {
            int hour, min, sec;
            hour = min = sec = -1;
            bool fail1, fail2, fail3;
            fail1 = fail2 = fail3 = true;

            string h_tk = tokens_here[tokenTposition + 1],
                   min_tk = tokens_here[tokenTposition + 2],
                   s_tk = tokens_here[tokenTposition + 3];

            if (int.TryParse(h_tk, out hour)) //година
            {
                fail1 = hour < 0 || hour > 23;
                if (fail1) {
                    Console.WriteLine("Помилка - година може бути в інтервалі від 0 до 23 включно");
                }
                else { DateTimeComponents[3] = hour; }
            }
            else {
                fail1 = h_tk != ".";
                if (fail1) { 
                    Console.WriteLine($"Помилка - значення для годин {tokens_here[tokenTposition + 1]} не є числом!");
                }             
            }

            if (int.TryParse(min_tk, out min)) //хвилина
            {
                fail2 = min < 0 || min > 59;
                if (fail2) {
                    Console.WriteLine("Помилка - хвилина може бути в інтервалі від 0 до 59 включно");
                }
                else { DateTimeComponents[4] = min; }
            }
            else {
                fail2 = min_tk != ".";
                if (fail2) {
                    Console.WriteLine($"Помилка - значення для хвилин {tokens_here[tokenTposition + 2]} не є числом!");
                }
            }

            if (int.TryParse(s_tk, out sec))
            { 
                fail3 = sec < 0 || sec > 59;
                if (fail3) {
                    Console.WriteLine("Помилка - секунда може бути в інтервалі від 0 до 59 включно");
                }
                else  { DateTimeComponents[5] = sec; }
            }
            else {
                fail3 = s_tk != ".";
                if (fail3) {
                    Console.WriteLine($"Помилка - значення для секунд {tokens_here[tokenTposition + 1]} не є числом!");
                }
            }

            if (DateTimeComponents[3]==null && DateTimeComponents[4] == null && DateTimeComponents[5] == null)
            {
                Console.WriteLine("Помилка - всі компоненти часу пропущені!"); return true;
            }

            return fail1 || fail2 || fail3;
        }

        static void Main(string[] args)
        {
            Console.OutputEncoding = Console.InputEncoding = Encoding.Unicode;

            if (args.Length == 0)
            {
                Console.WriteLine("Помилка - нічого не передано\nякщо ви шукаєте підказку - запустіть ще раз з параметром help");
                Environment.Exit((int)ErrorCodes.TooFewParameters);
            }
            else if (args[0].ToLower() == "help")
            {
                Console.WriteLine(ReturnHelpText()); return;
            }
            else if (args.Length==1)
            {
                Console.WriteLine("Помилка - недостатньо парамерів\nКрім папки, ще треба зазначити щонайменше дату або час");
                Environment.Exit((int)ErrorCodes.TooFewParameters);
            }

            if (!Directory.Exists(args[0]))
            {
                Console.WriteLine($"Не знайдено папки {args[0]}");
                Environment.Exit((int)ErrorCodes.DirNotFound);
            }

            DateTimeComponents = new int?[6];
            int crt_arg = 1;
            List<string> extensions = null;
            BadDateHandle handle = BadDateHandle.SkipDate;
            bool dont_go_to_subdirs = false, 
                change_date = false,
                change_time = false;
            FileAttributes attrs_to_skip = 0;

            do
            {
                switch (args[crt_arg])
                {
                    case "-d":
                        if (args.Length < crt_arg + 4)
                        {
                            Console.WriteLine("Помилка - зазначено недостатньо компонентів дати! " +
                                              "Якщо ви хотіли їх пропустити - використовуйте крапки на їх місці (див. help)");
                            Environment.Exit((int)ErrorCodes.TooFewParameters);
                        }

                        if (FilterOutInvalidDate(args, crt_arg)) { Environment.Exit((int)ErrorCodes.InvalidArgument); }
                        change_date = true;
                        crt_arg += 3;
                        break;
                    case "-t":
                        if (args.Length < crt_arg + 4)
                        {
                            Console.WriteLine("Помилка - зазначено недостатньо компонентів часу! " +
                                              "Якщо ви хотіли їх пропустити - використовуйте крапки на їх місці (див. help)");
                            Environment.Exit((int)ErrorCodes.TooFewParameters);
                        }
                        if (FilterOutInvalidTime(args, crt_arg)) { Environment.Exit((int)ErrorCodes.InvalidArgument); }
                        change_time = true;
                        crt_arg += 3;
                        break;
                    case "-ext": //шаблон розширення
                        if (args.Length <= crt_arg || args[crt_arg + 1][0] == '-')
                        {
                            Console.WriteLine($"Помилка - не зазначені параметри опції -ext (розширення файлів)");
                            Environment.Exit((int)ErrorCodes.TooFewParameters);
                        }
                        else {
                            if (extensions == null) { extensions = new List<string>(); }
                            
                            do {                        
                                crt_arg++;
                                string ext = args[crt_arg][0]=='.'? args[crt_arg] : $".{args[crt_arg]}";

                                if (extensions.Count == 0 || extensions[extensions.Count-1]!=ext)  { 
                                    extensions.Add(ext); 
                                }
                            }
                            while (crt_arg < args.Length-1 && args[crt_arg + 1][0] != '-');
                        }
                        break;
                    case "-bd": //якщо утворилася недопустима дата
                        if (args.Length <= crt_arg || args[crt_arg + 1][0] == '-')
                        {
                            Console.WriteLine($"Помилка - не зазначені параметри опції -bd");
                            Environment.Exit((int)ErrorCodes.TooFewParameters);
                        }
                        else {
                            crt_arg++;
                            switch (args[crt_arg])
                            {
                                case "skf":
                                    handle = BadDateHandle.SkipFile; break;
                                case "skd":
                                    handle = BadDateHandle.SkipDate; break;
                                case "rnd":
                                    handle = BadDateHandle.RoundDate; break;
                                case "err":
                                    handle = BadDateHandle.Error; break;
                                default:
                                    Console.WriteLine($"Помилка - невідомий параметр {args[crt_arg]} для опції -bd");
                                    Environment.Exit((int)ErrorCodes.InvalidArgument);
                                    break;
                            }
                        }
                        break;
                    case "-nosd": //не зачіпати підпапки та файли у них
                        dont_go_to_subdirs = true;
                        break;
                    case "-sk": //пропускати з якимись атрибутами
                        if (args.Length <= crt_arg || args[crt_arg + 1][0] == '-')
                        {
                            Console.WriteLine($"Помилка - не зазначені параметри опції -sk (пропускати з атрибутами)");
                            Environment.Exit((int)ErrorCodes.TooFewParameters);
                        }
                        else {
                            do {
                                FileAttributes crt = 0;
                                crt_arg++;
                                switch (args[crt_arg])
                                {
                                    case "r":
                                        crt = FileAttributes.ReadOnly; break;
                                    case "a":
                                        crt = FileAttributes.Archive; break;
                                    case "h":
                                        crt = FileAttributes.Hidden; break;
                                    case "s":
                                        crt = FileAttributes.System; break;
                                    default:
                                        Console.WriteLine($"Помилка - неприпустимий параметр {args[crt_arg]} для опції -sk");
                                        Environment.Exit((int)ErrorCodes.InvalidArgument);
                                        break;
                                }
                                attrs_to_skip = attrs_to_skip | crt;
                            }
                            while (crt_arg < args.Length - 1 && args[crt_arg + 1][0] != '-');                            
                        }
                        break;
                    default:
                        Console.WriteLine($"Невідомий параметр {args[crt_arg]}");
                        Environment.Exit((int)ErrorCodes.InvalidParameter);
                        break;
                }
                crt_arg++;
            }
            while (crt_arg < args.Length);

            if(!change_time && !change_date)
            {
                Console.WriteLine("Помилка - не зазначено ні дати, ні часу");
                Environment.Exit((int)ErrorCodes.TooFewParameters);
            }

            string[] files = Directory.GetFiles(args[0], "*.*", dont_go_to_subdirs ? SearchOption.TopDirectoryOnly : SearchOption.AllDirectories);
            

            for(int i =0; i < files.Length; ++i)
            {
                if (extensions!=null && !extensions.Contains(Path.GetExtension(files[i]))) //фільтр за розширенням файлу
                {
                    continue;
                }
                FileAttributes fa = File.GetAttributes(files[i]);
                if ((fa & attrs_to_skip) != 0) //якщо перетин цих множин атрибутів не порожній
                {
                    Console.WriteLine($"Файл {files[i]} пропущений через його атрибути");
                    continue;
                }

                DateTime crt_orig_date = File.GetCreationTime(files[i]);
                int crt_day = DateTimeComponents[0] == null ? crt_orig_date.Day : DateTimeComponents[0].Value, 
                      crt_month = DateTimeComponents[1] == null ? crt_orig_date.Month : DateTimeComponents[1].Value, 
                      crt_year = DateTimeComponents[2] == null ? crt_orig_date.Year : DateTimeComponents[2].Value;

                if(change_date && !CheckPotentialDate(crt_day, crt_month, crt_year))
                {
                    switch(handle)
                    {
                        case BadDateHandle.SkipFile:
                            Console.WriteLine($"Файл {files[i]} пропущений, бо для нього утворилася недопустима дата - {crt_day}.{crt_month:00}.{crt_year}");
                            continue;
                            break;
                        case BadDateHandle.Error:
                            Console.WriteLine($"Помилка - для файла {files[i]} утворилася недопустима дата - {crt_day}.{crt_month:00}.{crt_year}");
                            Environment.Exit((int)ErrorCodes.InvalidFunction);
                            break;
                        case BadDateHandle.SkipDate:
                            Console.WriteLine($"Для файла {files[i]} утворилася недопустима дата - {crt_day}.{crt_month:00}.{crt_year}."
                                                + "\n\tЙого дату створення не буде змінено");
                            if (!change_time) {
                                continue;
                            }
                            crt_day = crt_orig_date.Day;
                            crt_month = crt_orig_date.Month;
                            crt_year = crt_orig_date.Year;
                            break;
                        case BadDateHandle.RoundDate:
                            if(crt_month==2)
                            {
                                crt_day = crt_year % 4 == 0 ? 29 : 28;
                            }
                            else if(crt_month == 4 || crt_month == 6 || crt_month ==9 || crt_month == 11) { crt_day = 30; }
                            Console.Write($"(дата виправлена на {crt_day}.{crt_month:00}.{crt_year}) ");
                            break;
                    }
                }

                DateTime changed = new DateTime(crt_year, crt_month, crt_day,
                                                DateTimeComponents[3] == null ? crt_orig_date.Hour : DateTimeComponents[3].Value,
                                                DateTimeComponents[4] == null ? crt_orig_date.Minute : DateTimeComponents[4].Value,
                                                DateTimeComponents[5] == null ? crt_orig_date.Second : DateTimeComponents[5].Value);
                File.SetCreationTime(files[i], changed);
                Console.WriteLine($"Успішно змінено для файла {files[i]}");
            }
        }
    }
}
